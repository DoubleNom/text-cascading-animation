import 'dart:async';

import 'package:flutter/material.dart';
import 'package:text_animation/separated_widget_list.dart';

class TextCascadingAnimation extends StatefulWidget {
  final String text;

  const TextCascadingAnimation(this.text, {super.key});

  @override
  State<TextCascadingAnimation> createState() => _TextCascadingAnimationState();
}

class _TextCascadingAnimationState extends State<TextCascadingAnimation>
    with TickerProviderStateMixin {
  late List<String> words = widget.text.split(" ");

  late final List<AnimationController> _slideControllers = List.generate(
    words.length,
    (index) => AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    ),
  );

  late final List<AnimationController> _opacityControllers = List.generate(
    words.length,
    (index) => AnimationController(
        duration: const Duration(milliseconds: 100), vsync: this),
  );

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < words.length; ++i) {
      _slideControllers[i].addStatusListener((status) {
        if (status == AnimationStatus.forward) {
          _opacityControllers[i].forward();
          if (i == words.length - 1) return;
          Timer(const Duration(milliseconds: 50),
              () => _slideControllers[i + 1].forward());
        }
      });
    }
    _slideControllers.last.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        Timer(const Duration(seconds: 1), () {
          for (var element in _opacityControllers) {
            element.reset();
          }
          for (var element in _slideControllers) {
            element.reset();
          }
          _slideControllers.first.forward();
        });
      }
    });
    _slideControllers.first.forward();
  }

  @override
  void dispose() {
    for (var controller in _slideControllers) {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Wrap(
        children: separatedWidgetList(
          List.generate(
            words.length,
            (index) => FadeTransition(
              opacity: Tween<double>(begin: 0, end: 1).animate(
                _opacityControllers[index],
              ),
              child: SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(0, -1),
                  end: Offset.zero,
                ).animate(_slideControllers[index]),
                child: Text(words[index]),
              ),
            ),
          ),
          2,
        ),
      );
}
