import 'package:flutter/material.dart';
import 'package:text_animation/text_cascading_animation.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const Scaffold(
        body: Center(
          child: TextCascadingAnimation(
            "Ceci est un message très long avec tout pleins de mots",
          ),
        ),
      ),
    );
  }
}
