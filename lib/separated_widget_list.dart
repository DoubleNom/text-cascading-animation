import 'package:flutter/material.dart';

List<Widget> separatedWidgetList(List<Widget> widgets, double gap) =>
    List.generate(
      widgets.length * 2 - 1,
      (index) => index.isEven
          ? widgets[index ~/ 2]
          : SizedBox.square(
              dimension: gap,
            ),
    );
